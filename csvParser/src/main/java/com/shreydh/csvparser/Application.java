package com.shreydh.csvparser;

import java.io.IOException;

/**
 * Java application for reading csv file and logging into database.
 * @author : Shrey Dhungana
 */

public class Application {

    public static void main(String[] args) throws  IOException{

        final long startTime = System.nanoTime();

        String filename = "ms3Interview.csv";
        String sqlScript = "example.sql";

        DatabaseUtil dbStart = new DatabaseUtil();

        /*The sql script to create table for storing csv file records*/
        dbStart.runSQlScript(sqlScript);

        /*Read the csv file*/
        /*Read CSV file*/
        CsvReader fileReader = new CsvReader();
        fileReader.readCSVfile(filename);

        /*select statement to query database for test purpose*/
        dbStart.selectAll();

        final long duration = System.nanoTime() - startTime;
        System.out.println("Total time is :"+duration);

    }

}
