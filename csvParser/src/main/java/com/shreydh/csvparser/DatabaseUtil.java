package com.shreydh.csvparser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.util.List;

/**
 * This class performs database operations initialization,table creation, insertion and selection.
 */

public class DatabaseUtil {

    public DatabaseUtil(){

    }

    /**
     * Method to create sqlite database connection
     * @return Connection object
     */

    public Connection connect() {

        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite::memory:testDB";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

           // System.out.println("Connection to SQLite has been established."+conn.getMetaData().getURL());

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    /**
     * This method runs the sql script
     * @param filename: filename containing the sql script
     */
    public void runSQlScript(String filename) {

        // SQL statement for creating a new table from example.sql
        String sql = queryBuilder(filename);
        try{
            Connection conn = this.connect();
            Statement stmt = conn.createStatement();
            // create a new table
            stmt.execute(sql);
        } catch (SQLException e) {
           e.printStackTrace();
        }
    }

    /**
     * This method builds sql query from a given .sql file
     * @param aSQLScriptFilePath : a sql script file
     * @return sqlString : sql command from the file
     */
    private String queryBuilder(String aSQLScriptFilePath)  {

        StringBuffer sb = null;
        try {
            BufferedReader in = new BufferedReader(new FileReader(aSQLScriptFilePath));
            String str;
            sb = new StringBuffer();
            while ((str = in.readLine()) != null) {
                sb.append(str + "\n ");
            }

            in.close();
        } catch (Exception e) {
            System.err.println("Failed to Execute" + aSQLScriptFilePath +". The error is"+ e.getMessage());
        }
        String sqlString = sb.toString();
        if(sqlString != null)
            return  sqlString;
        return " ";

    }

    /**
     * This method is for querying the database and running update script
     */
    public void selectAll(){

        String sql = "SELECT * FROM storage" ;

        String sqlDrop = "DROP TABLE IF EXISTS storage";

        try {
            Connection conn = this.connect();

             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            System.out.println("rs size is"+rs.getFetchSize());
            while (rs.next()) {
                System.out.println(rs.getString("A") +  "\t" +
                        rs.getString("B") + "\t" +
                        rs.getString("C") + "\t" +
                        rs.getString("D") + "\t" +
                        rs.getString("E") + "\t" +
                        rs.getString("F") + "\t" +
                        rs.getString("G") + "\t" +
                        rs.getString("I") + "\t" +
                        rs.getString("J"));
            }

            stmt.executeUpdate(sqlDrop);
            if(conn != null)
                conn.close();
            if(rs !=null)
                rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * This method inserts records into database table called storage
     * @param myList : list of the records to be inserted into database
     */

    public void insert(List<CsvReader> myList) {

        String sql = "INSERT INTO storage(A,B,C,D,E,F,G,H,I,J) VALUES(?,?,?,?,?,?,?,?,?,?)";
        /*batch size for record insertion is 1000*/
        final int batchSize = 1000;
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = this.connect();
            pstmt = conn.prepareStatement(sql);

            /*Autocommit mode, in theory, incurs per-statement transaction overhead*/
            conn.setAutoCommit(false);
            int count = 0;

            for(int i = 0; i < myList.size(); i++) {

                CsvReader record = myList.get(i);
                pstmt.setString(1, record.getA());
                pstmt.setString(2, record.getB());
                pstmt.setString(3, record.getC());
                pstmt.setString(4, record.getD());
                pstmt.setString(5, record.getE());
                pstmt.setString(6, record.getF());
                pstmt.setString(7, record.getG());
                pstmt.setString(8, record.getH());
                pstmt.setString(9, record.getI());
                pstmt.setString(10, record.getJ());

                pstmt.addBatch();

                /*execute when batch size turns 1000*/
                if (++count % batchSize == 0) {
                    pstmt.executeBatch();
                }
            }
            pstmt.executeBatch();
            conn.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally{
            try {
                /*free up resources*/
                if (pstmt != null)
                    pstmt.close();
                if (conn != null)
                    conn.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }
    }
}
