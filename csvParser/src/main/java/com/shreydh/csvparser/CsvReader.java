package com.shreydh.csvparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * Class to read and parse csv file to insert into database.
 */

public class CsvReader{



    /**
     * Instance variables corresponding to the csv file attributes.
     */
    final static Logger logger = Logger.getLogger(CsvReader.class);

    private String a, b, c, d, e, f, g, h, i, j;
    private static int INVALID_COUNTER = 0;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public String getI() {
        return i;
    }

    public void setI(String i) {
        this.i = i;
    }

    public String getJ() {
        return j;
    }

    public void setJ(String j) {
        this.j = j;
    }

    /**
     * Explicit class constructor
     * @param a  @column
     * @param b  @column
     * @param c  @column
     * @param d  @column
     * @param e  @column
     * @param f  @column
     * @param g  @column
     * @param h  @column
     * @param i  @column
     * @param j  @column
     */

    private CsvReader(String a, String b, String c, String d, String e,
                      String f, String g, String h, String i, String j) {

        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    /*explicit default constructor */

    public CsvReader() {

    }

    /**
     * Method to read and parse file. This method validates column size and double quoted fields.
     * @param filename : name of the csv file to parse
     * @throws IOException : exception resulting from file open
     */
    public void readCSVfile(String filename) throws IOException {

        BufferedReader br = Files.newBufferedReader(Paths.get(filename));
        CSVParser csvParser = new CSVParser(br, CSVFormat.DEFAULT
                .withFirstRecordAsHeader()
                .withIgnoreHeaderCase()
                .withTrim());

        int validRecords = 0;
        List<CSVRecord> invalidRecordList = new ArrayList<>();
        List<CsvReader> validRecordList = new ArrayList<>();

        for (CSVRecord record : csvParser) {

            if (record.size() == 10 && record.isConsistent()) {

                validRecords++;
                String a = record.get("A");
                String b = record.get("B");
                String c = record.get("C");
                String d = record.get("D");
                String e = record.get("E");
                String f = record.get("F");
                String g = record.get("G");
                String h = record.get("H");
                String i = record.get("I");
                String j = record.get("J");

                CsvReader rowObject = new CsvReader(a, b, c, d, e, f, g, h, i, j);

                /*insert the object to valid list*/
                validRecordList.add(rowObject);

            } else {

                /*count invalid records*/
                INVALID_COUNTER++;

                /*add invalid records to the list*/
                invalidRecordList.add(record);
            }
        }
        /*call method insertToDatabase*/
        insertToDatabase(validRecordList);

        /*calls method writeInvalidRecord*/
        writeInvalidRecord(invalidRecordList, validRecords);

    }

    /**
     * Writes the invalid records to  bad-data-<timestamp>.csv
     * @param myRecord list of invalid record
     */
    private void writeInvalidRecord(List<CSVRecord> myRecord, int validRecords){

        int total_records = INVALID_COUNTER + validRecords;
        logger.debug("Invalid record count is : "+INVALID_COUNTER);
        logger.debug("Valid record count is : "+validRecords);
        logger.debug("Total record count is : "+total_records);

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String stamp =timestamp.toString();
        String filename = "bad-data-"+stamp+".csv";
        try (CSVPrinter printer = new CSVPrinter(new FileWriter(filename), CSVFormat.EXCEL)) {
            for(CSVRecord record : myRecord){
                printer.printRecord(record);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Method to insert list of csv records into database.
     * @param myList row record list
     *
     */
    private void insertToDatabase(List<CsvReader> myList) {

        DatabaseUtil dbUtil = new DatabaseUtil();
        if (myList != null) {
            dbUtil.insert(myList);
        }

    }

    @Override
    public String toString() {
        return "CsvReader{" +
                "a='" + a + '\'' +
                ", b='" + b + '\'' +
                ", c='" + c + '\'' +
                ", d='" + d + '\'' +
                ", e='" + e + '\'' +
                ", f='" + f + '\'' +
                ", g='" + g + '\'' +
                ", h='" + h + '\'' +
                ", i='" + i + '\'' +
                ", j='" + j + '\'' +
                '}';
    }

}
