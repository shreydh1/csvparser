##CSV file parser.

###For Compliation use following :
$ mvn clean install 

$ java -cp target/csvParser-1.0-SNAPSHOT-jar-with-dependencies.jar   com.shreydh.csvparser.Application  

####Short Description:
> Java Source Code is in src/main/java/com/shreydh/csvparser/*.java

> Application.java is the entry of the program. It runs the runSQLScript() method in DatabaseUtils with example.sql
and readCSVfile() in CsvReader with ms3Interview.csv file.

> CsvReader class reads csv file ./ms3Interview.csv using Apache.commons.io.csv*. This library takes care of parsing 
csv record, including double quoted values.Valid records are kept in the list and sent to database. Invalid records are written to file 
./bad-data-<timestamp>.csv.
Used Log4j for logging in the statistics of the record to ./log1.log. This file contains the log of the records.

> DatabaseUtil is the database utility class. Method connect() initializes the connection to in-memory sqlite database.
Method runSQLScript() runs the .sql file.In this case, ./example.sql which creates a table called storage to store csv
records. Method insert() takes in list of valid records and adds the elements to the table in the batch of 1000. Method 
selectAll() is for testing purpose to view the records in the database.



